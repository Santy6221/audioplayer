GAMEBOY Clásica

1. Botón B para reproducir el audio.
2. Botón A para pausar el audio.
3. Botones horizontales de la cruceta (derecho para adelantar 5 segundos, izquierdo para retroceder).
4. Botones verticales de la cruceta (arriba para aumentar volumen, abajo para disminuir).
