class Player{

    constructor(domElement, play, pause, fastFwd, fastBwd, volumeUp, VolumeDown){
        this.domElement = domElement;
        this.src = this.domElement.dataset.src;
        this.audio = new Audio(this.src);
        this.controls = [play, pause, fastFwd, fastBwd, volumeUp, VolumeDown];
        this.progress = this.domElement.querySelector(".cover .progress");
        this.initControls();
        this.initProgressActions(this.controls[2], this.controls[3]);
        this.initVolumeActions(this.controls[4], this.controls[5])
        this.audio.ontimeupdate = () => { this.updateUI(); }
        console.log(this.domElement.dataset.src);
    }

    initControls() {
        if(this.controls[0]!=null && this.controls[1]!=null){
            this.initPlay(this.controls);
        }
    }

    initPlay(domElement) {
        domElement[0].onclick = () =>{
            this.play();
        }
        domElement[1].onclick = () =>{
            this.pause();
        }
    }

    initProgressActions(ffd, fbd) {
        // const cover = this.domElement.querySelector(".cover");
        // cover.onclick = (e) => {
        //     const x = e.offsetX;
        //     const totalX = cover.clientWidth;
        //     const progress = x / totalX;
        //     this.setCurrentTime(progress);
        // };
        ffd.onclick = () =>{
            this.setCurrentTime(5);
        }

        fbd.onclick = () =>{
            this.setCurrentTime(-5);
        }
    }

    initVolumeActions(volUp, VolDown){
        volUp.onclick = () =>{
            if(this.audio.volume <= 0.9){
                this.setVolume(0.10)
            }
        }
        VolDown.onclick = () =>{
            if(this.audio.volume >= 0.10){
                this.setVolume(-0.10)
            }
        }
    }

    setVolume(vol){
        this.audio.volume = this.audio.volume+vol;
    }

    setCurrentTime(progress) {
        this.audio.currentTime = this.audio.currentTime+progress;
    }


    updateUI() {
        const total = this.audio.duration;
        const current = this.audio.currentTime;
        const progress = (current / total) * 100;
        this.progress.style.width = `${progress}%`;
    }



    play() {
        this.audio.play().then().catch(err => console.log(`Error al reproducir el archivo: ${err}`));
    }

    pause() {
        this.audio.pause();
    }




}