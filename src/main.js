const songsEl = document.querySelectorAll(".song");
const play = document.querySelector(".playBtn");
const pause = document.querySelector(".pauseBtn");
const fastFwd = document.querySelector(".ffd")
const fastBwd = document.querySelector(".fbd")
const volumeUp = document.querySelector(".volUp")
const volumeDown = document.querySelector(".volDn")
const songs = [];

songsEl.forEach(song => {
    songs.push(new Player(song, play, pause, fastFwd, fastBwd, volumeUp, volumeDown));
});